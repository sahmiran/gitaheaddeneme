﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GitDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            btn1Cikti.Text = "Red";
            btn1Cikti.Background = Brushes.Red;
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            btn2Cikti.Text = "Green";
            btn2Cikti.Background = Brushes.Green;
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            btn3Cikti.Text = "Blue";
            btn3Cikti.Background = Brushes.Blue;
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            btn4Cikti.Text = "Yellow";
            btn4Cikti.Background = Brushes.Yellow;
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            btn5Cikti.Text = "Gray";
            btn5Cikti.Background = Brushes.Gray;
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            btn6Cikti.Text = "Orange";
            btn6Cikti.Background = Brushes.Orange;
        }
    }
}
